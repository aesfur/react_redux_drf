from django.contrib.auth import get_user_model
from allauth.account.utils import perform_login
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter

User = get_user_model()


class DiffbotSocialAccountAdapter(DefaultSocialAccountAdapter):
    """
    If user exists, connect the account to the existing account and login
    """
    def pre_social_login(self, request, sociallogin):
        user = sociallogin.user
        if user.id:
            return
        try:
            customer = User.objects.get(email=user.email)
            sociallogin.state['process'] = 'connect'
            perform_login(request, customer, 'none')
        except User.DoesNotExist:
            pass

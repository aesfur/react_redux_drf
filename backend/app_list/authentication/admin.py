from django.contrib import admin
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin

from app_list.authentication.forms import RegisterForm

User = get_user_model()


@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    add_form = RegisterForm
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'),
         {'fields': ('username', 'first_name', 'last_name')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser')
        }),
        (_('Roles'), {
            'classes': ('collapse',), 'fields': (
                'groups', 'user_permissions')
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ["email", "is_superuser", "last_login"]
    list_display_links = ["email"]
    search_fields = ['username', 'email', 'first_name', 'last_name']
    ordering = ['email', "is_superuser", ]
    list_filter = ["is_superuser"]
    readonly_fields = ["last_login", "date_joined"]

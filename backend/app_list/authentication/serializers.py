from rest_framework import serializers
from rest_auth.serializers import UserDetailsSerializer


class KnoxSerializer(serializers.Serializer):
    token = serializers.CharField()
    user = UserDetailsSerializer()

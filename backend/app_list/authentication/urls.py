from allauth.account.views import ConfirmEmailView, email_verification_sent
from django.urls import path, include
from rest_auth.registration.views import VerifyEmailView
from rest_auth.views import (
    UserDetailsView, PasswordChangeView, PasswordResetView,
    PasswordResetConfirmView
)

from app_list.authentication.views import (
    KnoxLoginView, KnoxRegisterView, KnoxLogoutView, KnoxLogoutAllView,
    GoogleLogin, VkLogin, FacebookLogin
)

urlpatterns = [

    path('account/', include('allauth.urls')),

    path('google/', GoogleLogin.as_view(), name='google_login'),
    path('vk/', VkLogin.as_view(), name='vk_login'),
    path('facebook/', FacebookLogin.as_view(), name='fb_login'),

    path('login/', KnoxLoginView.as_view(), name='rest_login'),
    path('logout/', KnoxLogoutView.as_view(), name='rest_logout'),
    path('logout-all/', KnoxLogoutAllView.as_view(), name='rest_logout_all'),

    path('password/reset/', PasswordResetView.as_view(),
         name='rest_password_reset'),
    path('password/reset/confirm/', PasswordResetConfirmView.as_view(),
         name='rest_password_reset_confirm'),
    path('user/', UserDetailsView.as_view(), name='rest_user_details'),
    path('password/change/', PasswordChangeView.as_view(),
         name='rest_password_change'),

    path('register/', KnoxRegisterView.as_view(), name='rest_register'),
    path('verify-email/', VerifyEmailView.as_view(),
         name='rest_verify_email'),
    path('account-confirm-email/<str:key>/', ConfirmEmailView.as_view(),
         name='account_confirm_email'),

    path("confirm-email/", email_verification_sent,
         name="account_email_verification_sent"),
]

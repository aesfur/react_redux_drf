from django.conf import settings
from django.contrib.auth import logout
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from rest_auth.registration.serializers import SocialLoginSerializer
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_auth.app_settings import create_token
from rest_auth.views import LoginView, LogoutView
from rest_auth.registration.views import RegisterView
from allauth.account.utils import complete_signup
from allauth.account import app_settings as allauth_settings
from allauth.account.adapter import get_adapter
from allauth.socialaccount.providers.facebook.views import (
    FacebookOAuth2Adapter
)
from allauth.socialaccount.providers.vk.views import VKOAuth2Adapter
from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from knox.settings import knox_settings

from app_list.authentication.serializers import KnoxSerializer


class KnoxLoginView(LoginView):

    def get_response(self):
        serializer_class = self.get_response_serializer()

        data = {
            'user': self.user,
            'token': self.token
        }
        serializer = serializer_class(
            instance=data,
            context={'request': self.request}
        )

        return Response(serializer.data)

    def post(self, request, *args, **kwargs):

        self.serializer = self.get_serializer(
            data=self.request.data,
            context={'request': request})
        self.serializer.is_valid(raise_exception=True)
        user = self.serializer.validated_data['user']

        token_limit_per_user = knox_settings.TOKEN_LIMIT_PER_USER
        if token_limit_per_user is not None:
            now = timezone.now()
            token = user.auth_token_set.filter(expires__gt=now)
            if token.count() >= token_limit_per_user:
                user.auth_token_set.all().delete()

        self.login()
        return self.get_response()


class KnoxRegisterView(RegisterView):

    def get_response_data(self, user):
        if (allauth_settings.EMAIL_VERIFICATION ==
                allauth_settings.EmailVerificationMethod.MANDATORY):
            return {"detail": _("Verification e-mail sent.")}
        else:
            return KnoxSerializer({'user': user, 'token': self.token}).data

    def perform_create(self, serializer):
        user = serializer.save(self.request)
        self.token = create_token(self.token_model, user, serializer)
        complete_signup(
            self.request._request,
            user,
            allauth_settings.EMAIL_VERIFICATION,
            None
        )

        return user


class KnoxLogoutView(LogoutView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        if getattr(settings, 'ACCOUNT_LOGOUT_ON_GET', False):
            response = self.logout(request)
        else:
            response = self.http_method_not_allowed(request, *args, **kwargs)
        return self.finalize_response(request, response, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.logout(request)

    def logout(self, request):
        try:
            request._auth.delete()
        except (AttributeError, ObjectDoesNotExist):
            pass

        logout(request)

        return Response({"detail": _("Successfully logged out.")})


class KnoxLogoutAllView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        if getattr(settings, 'ACCOUNT_LOGOUT_ON_GET', False):
            response = self.logout_all(request)
        else:
            response = self.http_method_not_allowed(request, *args, **kwargs)
        return self.finalize_response(request, response, *args, **kwargs)

    def post(self, request):
        return self.logout_all(request)

    def logout_all(self, request):
        try:
            request.user.auth_token_set.all().delete()
        except (AttributeError, ObjectDoesNotExist):
            pass

        logout(request)

        return Response({"detail": _("Successfully logged out.")})


class FacebookLogin(KnoxLoginView):
    adapter_class = FacebookOAuth2Adapter
    serializer_class = SocialLoginSerializer

    def process_login(self):
        get_adapter(self.request).login(self.request, self.user)


class GoogleLogin(KnoxLoginView):
    adapter_class = GoogleOAuth2Adapter
    serializer_class = SocialLoginSerializer

    def process_login(self):
        get_adapter(self.request).login(self.request, self.user)


class VkLogin(KnoxLoginView):
    adapter_class = VKOAuth2Adapter
    serializer_class = SocialLoginSerializer

    def process_login(self):
        get_adapter(self.request).login(self.request, self.user)

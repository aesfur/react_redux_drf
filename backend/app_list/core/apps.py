from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'app_list.core'

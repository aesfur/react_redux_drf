from django_filters import rest_framework as filters


class FilterBackend(filters.DjangoFilterBackend):

    def filter_queryset(self, request, queryset, view):
        filterset = super().filter_queryset(request, queryset, view)
        tags = request.query_params.get('tags', None)
        if tags:
            tags = [tag.strip() for tag in tags.split(',')]
            filterset = filterset.filter(tags__name__in=tags).distinct()

        return filterset

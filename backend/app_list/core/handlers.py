from rest_framework.views import exception_handler
from rest_framework.response import Response


def drf_exception_handler(exc, context):
    response = exception_handler(exc, context)

    unified_response = {}
    errors = []

    if response is not None:
        if hasattr(exc, 'detail') and hasattr(exc.detail, 'items'):
            for key, value in exc.detail.items():
                if key == 'non_field_errors':
                    error = ' '.join(value)
                else:
                    error = '{}: {}'.format(key, ' '.join(value))
                errors.append(error)
        elif response.data.get('detail'):
            errors.append(response.data.get('detail'))

        unified_response['status_code'] = response.status_code

    else:
        print(exc)  # TODO: trigger sentry
        response = Response(status=500)
        unified_response['status_code'] = 500
        errors.append("Unexpected server error")

    unified_response['errors'] = errors
    response.data = unified_response

    return response

from django.contrib.auth import get_user_model
from django.db import models
from taggit.managers import TaggableManager

User = get_user_model()


class Post(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(null=True)
    preview = models.ImageField(null=True)
    content = models.TextField()
    author = models.ForeignKey(User, on_delete=models.PROTECT, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    tags = TaggableManager(blank=True)

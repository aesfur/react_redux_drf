from rest_framework import serializers
from taggit_serializer.serializers import (
    TagListSerializerField, TaggitSerializer
)

from app_list.core.models import Post


class PostSerializer(TaggitSerializer, serializers.ModelSerializer):
    tags = TagListSerializerField()
    author_name = serializers.CharField(read_only=True, source="author")

    class Meta:
        model = Post
        fields = [
            "id", "title", "description", "preview", "content",
            "created_at", "tags", "author", "author_name"
        ]


class TagListSerializer(serializers.Serializer):
    tags = serializers.ListField(
        child=serializers.CharField()
    )

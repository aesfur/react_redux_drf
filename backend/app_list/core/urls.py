from django.urls import path
from rest_framework import routers

from app_list.core import views

router = routers.SimpleRouter()
router.register(r"posts", views.PostViewSet)

urlpatterns = [
    path('tags/', views.TagView.as_view(), name='tags'),
]

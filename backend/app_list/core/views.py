from rest_framework import viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.generics import ListAPIView
from rest_framework.parsers import JSONParser, MultiPartParser, FormParser
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from taggit.models import Tag

from app_list.core.models import Post
from app_list.core.serializers import PostSerializer, TagListSerializer


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    parser_classes = [JSONParser, MultiPartParser, FormParser]
    permission_classes = [IsAuthenticatedOrReadOnly]
    filterset_fields = ['author']
    search_fields = ['title']
    ordering = ['-created_at']

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.author != request.user:
            raise PermissionDenied("You can't edit this post")
        return super().update(request, *args, **kwargs)


class TagView(ListAPIView):
    queryset = Tag.objects.all()
    serializer_class = TagListSerializer

    def get(self, request, **kwargs):
        queryset = self.get_queryset().values_list("name", flat=True)
        serializer = self.get_serializer({"tags": queryset})
        return Response(serializer.data)

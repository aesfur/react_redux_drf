from split_settings.tools import optional, include

include(
    'settings/base.py',
    'settings/auth.py',
    'settings/db.py',
    optional('settings_local.py')
)

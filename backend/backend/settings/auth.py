from datetime import timedelta

AUTH_USER_MODEL = 'authentication.User'

ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_UNIQUE_EMAIL = True

ACCOUNT_EMAIL_VERIFICATION = "none"

# ACCOUNT_CONFIRM_EMAIL_ON_GET = True


AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'knox.auth.TokenAuthentication',
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated'
    ],
    'EXCEPTION_HANDLER': 'app_list.core.handlers.drf_exception_handler',
    'DEFAULT_FILTER_BACKENDS': [
        'app_list.core.filters.FilterBackend',
        'rest_framework.filters.SearchFilter',
        'rest_framework.filters.OrderingFilter'
    ]
}

REST_KNOX = {
    'TOKEN_LIMIT_PER_USER': 5,
    'TOKEN_TTL': timedelta(days=30),
    'USER_SERIALIZER': 'rest_auth.serializers.UserDetailsSerializer',
}

REST_AUTH_TOKEN_MODEL = 'knox.models.AuthToken'
REST_AUTH_TOKEN_CREATOR = 'app_list.authentication.utils.create_knox_token'
REST_SESSION_LOGIN = False

REST_AUTH_SERIALIZERS = {
    'USER_DETAILS_SERIALIZER': 'rest_auth.serializers.UserDetailsSerializer',
    'TOKEN_SERIALIZER': 'app_list.authentication.serializers.KnoxSerializer',
}

SOCIALACCOUNT_ADAPTER = 'app_list.authentication.adapter.DiffbotSocialAccountAdapter'

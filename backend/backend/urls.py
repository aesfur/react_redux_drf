from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view

from app_list.core.urls import router

schema_view = get_swagger_view(title='React Redux DRF')

urlpatterns = [
    path('api/', include(router.urls)),
    path('api/', include('app_list.core.urls')),
    path('api/schema/', schema_view),
    path('api/schema-auth/', include(
        'rest_framework.urls', namespace='rest_framework')),
    path('api/auth/', include('app_list.authentication.urls')),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )

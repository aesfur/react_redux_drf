import history from '../history';
import { ALERT_CLEAR } from './types';

export const clearAlerts = () => async dispatch => {
  history.listen((location, action) => {
    dispatch({ type: ALERT_CLEAR });
  });
};

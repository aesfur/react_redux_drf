import api, { getHeader, errorHandler } from '../api';
import history from '../history';
import {
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
  GET_USER_SUCCESS,
  REGISTER_SUCCESS,
  AUTH_LOADING,
  AUTH_ERROR,
} from './types';

export const register = formValues => async dispatch => {
  dispatch({ type: AUTH_LOADING });

  try {
    const response = await api.post('/auth/register/', formValues);
    localStorage.setItem('token', response.data.token);
    dispatch({ type: REGISTER_SUCCESS, payload: response.data });
    history.push('/');
  } catch (e) {
    dispatch({ type: AUTH_ERROR });
    errorHandler(dispatch, e);
  }
};

export const login = formValues => async dispatch => {
  dispatch({ type: AUTH_LOADING });

  try {
    const response = await api.post('/auth/login/', formValues);
    localStorage.setItem('token', response.data.token);
    dispatch({ type: LOGIN_SUCCESS, payload: response.data });
    history.push('/');
  } catch (e) {
    dispatch({ type: AUTH_ERROR });
    errorHandler(dispatch, e);
  }
};

export const logout = () => async dispatch => {
  dispatch({ type: AUTH_LOADING });

  try {
    await api.post('/auth/logout/', null, { headers: getHeader() });
    dispatch({ type: LOGOUT_SUCCESS });
    localStorage.removeItem('token');
    history.push('/login');
  } catch (e) {
    dispatch({ type: AUTH_ERROR });
    errorHandler(dispatch, e);
  }
};

export const logoutAll = () => async dispatch => {
  dispatch({ type: AUTH_LOADING });

  try {
    await api.post('/auth/logout-all/', null, { headers: getHeader() });
    dispatch({ type: LOGOUT_SUCCESS });
    localStorage.removeItem('token');
    history.push('/login');
  } catch (e) {
    dispatch({ type: AUTH_ERROR });
    errorHandler(dispatch, e);
  }
};

export const getUser = () => async (dispatch, getState) => {
  if (!getState().auth.token) {
    dispatch({ type: LOGOUT_SUCCESS });
    return;
  }

  dispatch({ type: AUTH_LOADING });

  try {
    const response = await api.get('/auth/user/', { headers: getHeader() });
    dispatch({ type: GET_USER_SUCCESS, payload: response.data });
  } catch (e) {
    dispatch({ type: AUTH_ERROR });
    errorHandler(dispatch, e);
  }
};

export const googleLogin = accessToken => async dispatch => {
  dispatch({ type: AUTH_LOADING });

  try {
    const response = await api.post('/auth/google/', {
      access_token: accessToken,
    });
    localStorage.setItem('token', response.data.token);
    dispatch({ type: LOGIN_SUCCESS, payload: response.data });
    history.push('/');
  } catch (e) {
    dispatch({ type: AUTH_ERROR });
    errorHandler(dispatch, e);
  }
};

export const facebookLogin = accessToken => async dispatch => {
  dispatch({ type: AUTH_LOADING });

  try {
    const response = await api.post('/auth/facebook/', {
      access_token: accessToken,
    });
    localStorage.setItem('token', response.data.token);
    dispatch({ type: LOGIN_SUCCESS, payload: response.data });
    history.push('/');
  } catch (e) {
    dispatch({ type: AUTH_ERROR });
    errorHandler(dispatch, e);
  }
};

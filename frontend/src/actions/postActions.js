import api, { getHeader, errorHandler } from '../api';
import history from '../history';
import {
  GET_POSTS_SUCCESS,
  GET_POST_SUCCESS,
  CREATE_POST_SUCCESS,
  DELETE_POST_SUCCESS,
  GET_TAGS_SUCCESS,
  LOADING,
  STOP_LOADING,
} from './types';

export const getPosts = () => async dispatch => {
  dispatch({ type: LOADING });

  try {
    const response = await api.get('/posts/', { headers: getHeader() });
    dispatch({ type: GET_POSTS_SUCCESS, payload: response.data });
  } catch (e) {
    dispatch({ type: STOP_LOADING });
    errorHandler(dispatch, e);
  }
};

export const getPost = id => async dispatch => {
  dispatch({ type: LOADING });

  try {
    const response = await api.get(`/posts/${id}`, { headers: getHeader() });
    dispatch({ type: GET_POST_SUCCESS, payload: response.data });
  } catch (e) {
    dispatch({ type: STOP_LOADING });
    errorHandler(dispatch, e);
  }
};

export const createPost = formValues => async (dispatch, getState) => {
  dispatch({ type: LOADING });

  let formData = new FormData(formValues);
  formData.append('author', getState().auth.user.pk);
  for (let key in formValues) {
    if (formValues.hasOwnProperty(key)) {
      formData.append(key, formValues[key]);
    }
  }

  try {
    const response = await api.post('/posts/', formData, {
      headers: getHeader(),
    });
    dispatch({ type: CREATE_POST_SUCCESS, payload: response.data });
    history.push(`/posts/${response.data.id}`);
  } catch (e) {
    dispatch({ type: STOP_LOADING });
    errorHandler(dispatch, e);
  }
};

export const editPost = formValues => async dispatch => {
  dispatch({ type: LOADING });

  let formData = new FormData(formValues);
  for (let key in formValues) {
    if (formValues.hasOwnProperty(key)) {
      formData.append(key, formValues[key]);
    }
  }

  try {
    const response = await api.patch(`/posts/${formValues.id}/`, formData, {
      headers: getHeader(),
    });
    dispatch({ type: CREATE_POST_SUCCESS, payload: response.data });
    history.push(`/posts/${response.data.id}`);
  } catch (e) {
    dispatch({ type: STOP_LOADING });
    errorHandler(dispatch, e);
  }
};

export const deletePost = id => async dispatch => {
  dispatch({ type: LOADING });

  try {
    await api.delete(`/posts/${id}`, { headers: getHeader() });
    dispatch({ type: DELETE_POST_SUCCESS, payload: id });
    history.push('/');
  } catch (e) {
    dispatch({ type: STOP_LOADING });
    errorHandler(dispatch, e);
  }
};

export const getTags = () => async dispatch => {
  try {
    const response = await api.get('/tags/', { headers: getHeader() });
    dispatch({ type: GET_TAGS_SUCCESS, payload: response.data });
  } catch (e) {
    errorHandler(dispatch, e);
  }
};

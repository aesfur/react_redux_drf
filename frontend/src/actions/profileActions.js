import api, { errorHandler, getHeader } from '../api';
import history from '../history';
import {
  ALERT_SUCCESS,
  GET_USER_SUCCESS,
  LOADING,
  STOP_LOADING,
} from './types';

export const updateUser = formValues => async dispatch => {
  dispatch({ type: LOADING });

  try {
    const response = await api.put('/auth/user/', formValues, {
      headers: getHeader(),
    });
    dispatch({ type: GET_USER_SUCCESS, payload: response.data });
    history.push('/profile');
  } catch (e) {
    dispatch({ type: STOP_LOADING });
    errorHandler(dispatch, e);
  }
};

export const changePassword = formValues => async dispatch => {
  dispatch({ type: LOADING });

  try {
    const response = await api.post('/auth/password/change/', formValues, {
      headers: getHeader(),
    });
    history.push('/profile');
    dispatch({ type: ALERT_SUCCESS, payload: response.data.detail });
  } catch (e) {
    errorHandler(dispatch, e);
  }

  dispatch({ type: STOP_LOADING });
};

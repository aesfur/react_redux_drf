import axios from 'axios';
import { ALERT_ERROR, AUTH_ERROR } from './actions/types';

export const getHeader = () => {
  const token = localStorage.getItem('token');
  return token ? { Authorization: 'Token ' + token } : {};
};

export const errorHandler = (dispatch, error) => {
  let messages = [];
  if (error.code === 'ECONNABORTED') {
    messages.push('Server is not responding');
  } else if (!error.response) {
    messages.push('Network error');
  } else {
    if (error.response.status === 401) {
      dispatch({ type: AUTH_ERROR });
      localStorage.removeItem('token');
      return false;
    } else {
      messages = error.response.data.errors;
    }
  }
  dispatch({
    type: ALERT_ERROR,
    payload: messages,
  });
};

export default axios.create({
  baseURL: 'http://localhost:8000/api/',
  timeout: 10000,
});

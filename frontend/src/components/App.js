import React from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import { Container, Message } from 'semantic-ui-react';
import { connect } from 'react-redux';

import TopMenu from './Menu';
import PostList from './post/PostList';
import PostDetail from './post/PostDetail';
import PostCreate from './post/PostCreate';
import PostEdit from './post/PostEdit';
import ProfileEdit from './profile/ProfileEdit';
import ChangePassword from './profile/ChangePassword';
import Login from './auth/Login';
import Profile from './profile/Profile';
import Register from './auth/Register';
import PrivateRoute from './auth/PrivatRoute';
import history from '../history';
import { clearAlerts } from '../actions/appActions';
import { getUser } from '../actions/authActions';

import 'braft-editor/dist/index.css';
import '../assets/main.css';

class App extends React.Component {
  componentDidMount() {
    this.props.clearAlerts();
    this.props.getUser();
  }

  static renderRouters() {
    return (
      <Switch>
        <Route path={'/login'} exact component={Login} />
        <Route path={'/register'} exact component={Register} />
        <Route path={'/'} exact component={PostList} />
        <PrivateRoute path={'/posts/new'} exact component={PostCreate} />
        <Route path={'/posts/:id'} exact component={PostDetail} />
        <PrivateRoute path={'/posts/:id/edit'} exact component={PostEdit} />
        <PrivateRoute path={'/profile'} exact component={Profile} />
        <PrivateRoute path={'/profile/edit'} exact component={ProfileEdit} />
        <PrivateRoute
          path={'/profile/password'}
          exact
          component={ChangePassword}
        />
      </Switch>
    );
  }

  renderMessages() {
    const { alerts } = this.props;
    return alerts.map((alert, index) => (
      <Message key={index} color={alert.color}>
        {alert.message}
      </Message>
    ));
  }

  render() {
    return (
      <React.Fragment>
        <Router history={history}>
          <React.Fragment>
            <TopMenu />
            <Container>
              {this.renderMessages()}
              {App.renderRouters()}
            </Container>
          </React.Fragment>
        </Router>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  alerts: state.alerts,
});

export default connect(
  mapStateToProps,
  { clearAlerts, getUser }
)(App);

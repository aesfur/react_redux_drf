import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Image, Dropdown, Menu, Icon } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { logout, googleLogin, facebookLogin } from '../actions/authActions';

class TopMenu extends React.Component {
  onLogout = () => {
    this.props.logout();
  };

  onGoogleSuccess = data => {
    this.props.googleLogin(data.accessToken);
  };

  onFacebookSuccess = data => {
    this.props.facebookLogin(data.accessToken);
  };

  renderGoogleButton() {
    return (
      <GoogleLogin
        clientId="34919540347-unj4k4fusbd8miphm5su7ehosqp3osbe.apps.googleusercontent.com"
        onSuccess={this.onGoogleSuccess}
        onFailure={e => console.log(e)}
        render={props => (
          <a className="item" {...props}>
            <Icon name="google" />
          </a>
        )}
      />
    );
  }

  renderFacebookButton() {
    return (
      <FacebookLogin
        appId="2075733695843105"
        fields="name,email,picture"
        callback={this.onFacebookSuccess}
        render={props => (
          <a href="#facebook" className="item" onClick={props.onClick}>
            <Icon name="facebook" />
          </a>
        )}
      />
    );
  }

  renderAuthBlock() {
    const { isAuthenticated, user } = this.props;
    return (
      <Menu.Menu position="right">
        {!isAuthenticated && (
          <React.Fragment>
            <Menu.Item as={Link} to={'/login'}>
              Login
            </Menu.Item>
            <Menu.Item as={() => this.renderGoogleButton()} />
            <Menu.Item as={() => this.renderFacebookButton()} />
          </React.Fragment>
        )}
        {isAuthenticated && (
          <React.Fragment>
            <Menu.Item as={Link} to={'/profile'}>
              {user.username || user.email}
            </Menu.Item>
            <Menu.Item onClick={this.onLogout}>Logout</Menu.Item>
          </React.Fragment>
        )}
      </Menu.Menu>
    );
  }

  render() {
    return (
      <Menu inverted stackable size="large" style={{ borderRadius: 0 }}>
        <Container>
          <Menu.Item as={Link} to={'/'} header>
            <Image
              size="mini"
              src="/favicon.ico"
              style={{ marginRight: '1.5em' }}
            />
            React Redux DRF
          </Menu.Item>
          <Menu.Item as={Link} to={'/'}>
            Posts
          </Menu.Item>
          <Menu.Item as={Link} to={'/posts/new'}>
            New
          </Menu.Item>

          <Dropdown item simple text="Dropdown">
            <Dropdown.Menu>
              <Dropdown.Item>List Item</Dropdown.Item>
              <Dropdown.Item>List Item</Dropdown.Item>
              <Dropdown.Divider />
              <Dropdown.Header>Header Item</Dropdown.Header>
              <Dropdown.Item>
                <i className="dropdown icon" />
                <span className="text">Submenu</span>
                <Dropdown.Menu>
                  <Dropdown.Item>List Item</Dropdown.Item>
                  <Dropdown.Item>List Item</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown.Item>
              <Dropdown.Item>List Item</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>

          {this.renderAuthBlock()}
        </Container>
      </Menu>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  user: state.auth.user,
});

export default connect(
  mapStateToProps,
  { logout, googleLogin, facebookLogin }
)(TopMenu);

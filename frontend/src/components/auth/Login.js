import React from 'react';
import { Link } from 'react-router-dom';
import { Header, Message, Grid } from 'semantic-ui-react';
import { connect } from 'react-redux';

import { login } from '../../actions/authActions';
import LoginForm from './LoginFrom';

class Login extends React.Component {
  render() {
    const { authLoading } = this.props;
    return (
      <Grid textAlign="center">
        <Grid.Column style={{ maxWidth: 600 }}>
          <Header as="h2" textAlign="center" size="large">
            Log-in to your account
          </Header>
          <LoginForm
            onSubmit={formValues => this.props.login(formValues)}
            loading={authLoading}
          />
          <Message size="large">
            New to us? <Link to="/register">Sign Up</Link>
          </Message>
        </Grid.Column>
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return {
    authLoading: state.auth.authLoading,
  };
};

export default connect(
  mapStateToProps,
  { login }
)(Login);

import React from 'react';
import { Button, Form, Segment } from 'semantic-ui-react';
import { Field, reduxForm } from 'redux-form';

import { Input } from '../formFields';

class LoginForm extends React.Component {
  onSubmit = formValues => {
    this.props.onSubmit(formValues);
  };

  render() {
    return (
      <Form
        loading={this.props.loading}
        onSubmit={this.props.handleSubmit(this.onSubmit)}
      >
        <Segment size="large">
          <Field
            fluid
            name="email"
            component={Input}
            icon="envelope"
            iconPosition="left"
            placeholder="Email"
          />
          <Field
            fluid
            name="password"
            component={Input}
            icon="lock"
            iconPosition="left"
            placeholder="Password"
            type="password"
          />
          <Button color="blue" fluid size="large">
            Login
          </Button>
        </Segment>
      </Form>
    );
  }
}

const validate = formValues => {
  const errors = {};

  if (!formValues.email) {
    errors.email = 'Please type in an username';
  }

  if (!formValues.password) {
    errors.password = 'Please enter a password';
  }

  if (formValues.password && formValues.password.length < 8) {
    errors.password = 'Your password must contain at least 8 characters';
  }

  return errors;
};

export default reduxForm({
  form: 'login',
  validate,
})(LoginForm);

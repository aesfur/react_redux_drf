import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Loader } from 'semantic-ui-react';

class PrivateRoute extends React.Component {
  render() {
    const {
      component: Component,
      isAuthenticated,
      authLoading,
      ...props
    } = this.props;
    return (
      <Route
        {...props}
        render={props => {
          if (authLoading) {
            return <Loader active />;
          } else if (!isAuthenticated) {
            return <Redirect to="/login" />;
          } else {
            return <Component {...props} />;
          }
        }}
      />
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  authLoading: state.auth.authLoading,
});

export default connect(mapStateToProps)(PrivateRoute);

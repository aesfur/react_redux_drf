import React from 'react';
import { Link } from 'react-router-dom';
import { Header, Message, Grid } from 'semantic-ui-react';
import { connect } from 'react-redux';

import { register } from '../../actions/authActions';
import RegisterForm from './RegisterForm';

class Register extends React.Component {
  render() {
    const { authLoading } = this.props;
    return (
      <Grid textAlign="center">
        <Grid.Column style={{ maxWidth: 600 }}>
          <Header as="h2" textAlign="center" size="large">
            Register account
          </Header>
          <RegisterForm
            onSubmit={formValues => this.props.register(formValues)}
            loading={authLoading}
          />
          <Message size="large">
            Already have an account? <Link to="/login">Sign In</Link>
          </Message>
        </Grid.Column>
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return {
    authLoading: state.auth.authLoading,
  };
};

export default connect(
  mapStateToProps,
  { register }
)(Register);

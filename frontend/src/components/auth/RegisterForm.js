import React from 'react';
import { Button, Form, Segment } from 'semantic-ui-react';
import { Field, reduxForm } from 'redux-form';

import { Input } from '../formFields';

class RegisterForm extends React.Component {
  onSubmit = formValues => {
    this.props.onSubmit(formValues);
  };

  render() {
    return (
      <Form
        loading={this.props.loading}
        onSubmit={this.props.handleSubmit(this.onSubmit)}
      >
        <Segment size="large">
          <Field
            fluid
            name="email"
            component={Input}
            icon="envelope"
            iconPosition="left"
            placeholder="Email"
          />
          <Field
            fluid
            name="password1"
            component={Input}
            icon="lock"
            iconPosition="left"
            placeholder="Password"
            type="password"
          />
          <Field
            fluid
            name="password2"
            component={Input}
            icon="lock"
            iconPosition="left"
            placeholder="Password confirmation"
            type="password"
          />
          <Button color="blue" fluid size="large">
            Submit
          </Button>
        </Segment>
      </Form>
    );
  }
}

const validate = formValues => {
  const errors = {};
  const { email, password1, password2 } = formValues;

  if (!email) {
    errors.email = 'Please type in an username';
  }

  if (!password1) {
    errors.password1 = 'Please enter a password';
  }

  if (!password2) {
    errors.password2 = 'Please enter a password';
  }

  if (password1 !== password2) {
    errors.password2 = 'Passwords are not equal';
  }

  if (password1 && password1.length < 8) {
    errors.password1 = 'Your password must contain at least 8 characters';
  }

  return errors;
};

export default reduxForm({
  form: 'register',
  validate,
})(RegisterForm);

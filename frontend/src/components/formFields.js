import React from 'react';
import { Form, Message, Button } from 'semantic-ui-react';
import uuid from 'uuid';

export const Input = ({ input, meta, ...props }) => (
  <React.Fragment>
    <Form.Input {...input} {...props} error={meta.touched && meta.invalid} />
    <ErrorMessage meta={meta} />
  </React.Fragment>
);

export const TextArea = ({ input, meta, ...props }) => (
  <React.Fragment>
    <Form.TextArea {...input} {...props} error={meta.touched && meta.invalid} />
    <ErrorMessage meta={meta} />
  </React.Fragment>
);

export const ErrorMessage = ({ meta }) => (
  <React.Fragment>
    {meta.touched && meta.invalid && (
      <Message negative size="tiny">
        <p>{meta.error}</p>
      </Message>
    )}
  </React.Fragment>
);

export class FileButton extends React.Component {
  constructor(props) {
    super(props);
    this.id = uuid.v1();
  }

  render() {
    const { fileName } = this.props;

    return (
      <React.Fragment>
        <Button
          as="label"
          htmlFor={this.id}
          size="large"
          icon="upload"
          content="Select image"
          labelPosition="left"
          color={fileName && 'blue'}
          basic
        />
        <input hidden id={this.id} type="file" onChange={this.onChangeFile} />
        {fileName && <Message icon="image" size="tiny" header={fileName} />}
      </React.Fragment>
    );
  }

  onChangeFile = () => {
    const fileButton = document.getElementById(this.id);
    const file = fileButton ? fileButton.files[0] : null;
    if (this.props.onSelect) {
      this.props.onSelect(file);
    }
  };
}

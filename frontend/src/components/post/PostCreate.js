import React from 'react';
import { Segment, Header, Grid } from 'semantic-ui-react';
import { connect } from 'react-redux';

import PostForm from './PostForm';
import { createPost, getTags } from '../../actions/postActions';

class PostCreate extends React.Component {
  componentDidMount() {
    this.props.getTags();
  }

  render() {
    return (
      <Grid>
        <Grid.Column>
          <Header
            as="h2"
            size="large"
            content="Post creation"
            subheader="Easy to build any type of rich text input"
          />
          <Segment>
            <PostForm
              loading={this.props.loading}
              suggestionTags={this.props.suggestionTags}
              onSubmit={formValues => this.props.createPost(formValues)}
            />
          </Segment>
        </Grid.Column>
      </Grid>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    loading: state.app.loading,
    suggestionTags: state.tags,
    post: state.posts[ownProps.match.params.id],
  };
};

export default connect(
  mapStateToProps,
  { createPost, getTags }
)(PostCreate);

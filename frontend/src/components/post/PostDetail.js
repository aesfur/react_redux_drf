import React from 'react';
import { Link } from 'react-router-dom';
import Editor from 'braft-editor';
import { connect } from 'react-redux';
import moment from 'moment';
import {
  Button,
  Confirm,
  Container,
  Header,
  Label,
  Loader,
  Segment,
  Grid,
  Divider,
} from 'semantic-ui-react';

import { getPost, deletePost } from '../../actions/postActions';

class PostDetail extends React.Component {
  state = { open: false };

  open = () => this.setState({ open: true });
  close = () => this.setState({ open: false });

  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getPost(id);
  }

  onDeleteClick = () => {
    const { id } = this.props.match.params;
    this.props.deletePost(id);
  };

  renderTags() {
    const { tags } = this.props.post;
    return (
      tags &&
      tags.map(tag => {
        return <Label key={tag} color="teal" basic content={tag} />;
      })
    );
  }

  renderPost() {
    const { title, content, created_at, author_name } = this.props.post;
    const contentState = Editor.createEditorState(content);
    return (
      <Container text>
        <Grid>
          <Grid.Column width="12">
            <Label.Group>{this.renderTags()}</Label.Group>
          </Grid.Column>
          <Grid.Column textAlign="right" width="4">
            <span className="ui text grey">{moment(created_at).fromNow()}</span>
          </Grid.Column>
        </Grid>
        <Header as="h2">{title}</Header>
        <Editor
          value={contentState}
          readOnly
          controls={[]}
          contentStyle={{ height: '100%' }}
        />
        <Divider />
        <Grid>
          <Grid.Column width="8">
            <span className="text blue">
              {moment(created_at).format('MMM YY')}
            </span>
          </Grid.Column>
          <Grid.Column textAlign="right" width="8">
            <span className="text blue"> {author_name}</span>
          </Grid.Column>
        </Grid>
      </Container>
    );
  }

  renderControls() {
    const { user, post } = this.props;
    if (user && user.pk === post.author) {
      return (
        <Button.Group basic icon floated="right">
          <Button icon="edit" as={Link} to={`/posts/${post.id}/edit`} />
          <Button icon="delete" onClick={this.open} />
        </Button.Group>
      );
    }
  }

  render() {
    return (
      <React.Fragment>
        {this.props.post && (
          <Segment>
            <Confirm
              open={this.state.open}
              onCancel={this.close}
              onConfirm={this.onDeleteClick}
              header="Delete this post"
              size="tiny"
              centered={false}
            />
            <Loader active={this.props.loading} />
            {this.renderControls()}
            {this.renderPost()}
          </Segment>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    user: state.auth.user,
    post: state.posts[ownProps.match.params.id],
    loading: state.app.loading,
  };
};

export default connect(
  mapStateToProps,
  { getPost, deletePost }
)(PostDetail);

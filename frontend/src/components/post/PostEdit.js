import React from 'react';
import { Segment, Header, Grid } from 'semantic-ui-react';
import { connect } from 'react-redux';

import PostForm from './PostForm';
import { editPost, getTags } from '../../actions/postActions';

class PostEdit extends React.Component {
  componentDidMount() {
    const { id } = this.props.match.params;
    if (!this.props.post) {
      this.props.history.push(`/posts/${id}`);
    }
    this.props.getTags();
  }

  getInitialValues(post) {
    return {
      id: post.id,
      title: post.title,
      description: post.description,
      content: post.content,
      tags: post.tags,
    };
  }

  render() {
    const { loading, suggestionTags, post, editPost } = this.props;
    return (
      <Grid>
        <Grid.Column>
          <Header
            as="h2"
            size="large"
            content="Post editing"
            subheader="Easy to build any type of rich text input"
          />
          <Segment>
            <PostForm
              loading={loading}
              suggestionTags={suggestionTags}
              initialValues={post && this.getInitialValues(post)}
              onSubmit={(formValues, id) => editPost(formValues, id)}
            />
          </Segment>
        </Grid.Column>
      </Grid>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    loading: state.app.loading,
    suggestionTags: state.tags,
    post: state.posts[ownProps.match.params.id],
  };
};

export default connect(
  mapStateToProps,
  { editPost, getTags }
)(PostEdit);

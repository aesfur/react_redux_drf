import React from 'react';
import { Grid, Button, Divider, Form, Header } from 'semantic-ui-react';
import { Field, reduxForm } from 'redux-form';
import Editor from 'braft-editor';
import Tags from 'react-chips';

import Tag from '../secondary/Tag';
import { FileButton, TextArea } from '../formFields';

class PostForm extends React.Component {
  state = {
    filePreview: null,
    content: Editor.createEditorState(null),
    tags: [],
  };

  componentDidMount() {
    const { initialValues } = this.props;
    const content = Editor.createEditorState(
      initialValues ? initialValues.content : null
    );
    const tags = initialValues ? initialValues.tags : [];
    this.setState({ content, tags });
  }

  onSubmit = formValues => {
    const { filePreview, tags, content } = this.state;
    formValues.tags = JSON.stringify(tags);
    formValues.content = content.toHTML();
    if (filePreview) {
      formValues.preview = this.state.filePreview;
    }
    this.props.onSubmit(formValues);
  };

  onPreviewSelect = file => {
    this.setState({
      filePreview: file,
    });
  };

  render() {
    const { filePreview, content, tags } = this.state;
    return (
      <Form size="large" loading={this.props.loading}>
        <Grid columns={2} divided>
          <Grid.Row>
            <Grid.Column>
              <Divider horizontal>
                <Header as="h4">Title</Header>
              </Divider>
              <Field
                name="title"
                component={TextArea}
                style={{ minHeight: 100 }}
                placeholder="Enter Title"
              />
              <Divider horizontal>
                <Header as="h4">Description</Header>
              </Divider>
              <Field
                name="description"
                component={TextArea}
                placeholder="Enter Description"
                style={{ minHeight: 100 }}
              />
            </Grid.Column>
            <Grid.Column>
              <Divider horizontal>
                <Header as="h4">Tags</Header>
              </Divider>
              <Tags
                value={tags}
                onChange={tags => this.setState({ tags })}
                suggestions={this.props.suggestionTags}
                createChipKeys={['Enter']}
                renderChip={tag => <Tag tag={tag} />}
                placeholder="Enter or select tags"
              />
              <Divider horizontal>
                <Header as="h4">Preview image</Header>
              </Divider>
              <FileButton
                onSelect={this.onPreviewSelect}
                fileName={filePreview && filePreview.name}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Editor
          value={content}
          onChange={content => this.setState({ content })}
          placeholder="Enter content"
          language="en"
        />
        <Divider />
        <div style={{ textAlign: 'center' }}>
          <Button
            size="large"
            color="blue"
            type="button"
            onClick={this.props.handleSubmit(this.onSubmit)}
          >
            Submit
          </Button>
        </div>
      </Form>
    );
  }
}

const validate = formValues => {
  const errors = {};

  if (!formValues.title) {
    errors.title = 'Please type in a title';
  }

  return errors;
};

export default reduxForm({
  form: 'postForm',
  validate,
})(PostForm);

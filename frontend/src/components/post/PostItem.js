import React from 'react';
import { Card, Image, Grid, Label } from 'semantic-ui-react';
import moment from 'moment';
import { Link } from 'react-router-dom';

class PostItem extends React.Component {
  renderTags = () => {
    const { tags } = this.props.post;
    return (
      tags &&
      tags.map(tag => {
        return <Label key={tag} color="teal" basic content={tag} />;
      })
    );
  };

  render() {
    const {
      id,
      title,
      description,
      author_name,
      preview,
      created_at,
    } = this.props.post;

    return (
      <Grid.Column>
        <Card as={Link} to={`posts/${id}`} fluid>
          <Image src={preview} />
          <Card.Content extra>
            <Grid columns="2">
              <Grid.Column>
                <span>{moment(created_at).fromNow()}</span>
              </Grid.Column>
              <Grid.Column textAlign="right">
                <span>{author_name}</span>
              </Grid.Column>
            </Grid>
          </Card.Content>
          <Card.Content>
            <Card.Header>{title}</Card.Header>
            <Card.Description>{description}</Card.Description>
          </Card.Content>
          <Card.Content extra>
            <Label.Group>{this.renderTags()}</Label.Group>
          </Card.Content>
        </Card>
      </Grid.Column>
    );
  }
}

export default PostItem;

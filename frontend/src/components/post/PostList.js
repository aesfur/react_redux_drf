import React from 'react';
import { connect } from 'react-redux';
import { Loader, Grid } from 'semantic-ui-react';

import PostItem from './PostItem';
import { getPosts } from '../../actions/postActions';

class PostList extends React.Component {
  componentDidMount() {
    this.props.getPosts();
  }

  renderPosts() {
    return this.props.posts.map(post => {
      return <PostItem key={post.id} post={post} />;
    });
  }

  render() {
    return (
      <Grid columns={2} doubling stackable className="masonry">
        <Loader active={this.props.loading} />
        {this.renderPosts()}
      </Grid>
    );
  }
}

const mapStateToProps = state => {
  return {
    posts: Object.values(state.posts),
    loading: state.app.loading,
  };
};

export default connect(
  mapStateToProps,
  { getPosts }
)(PostList);

import React from 'react';
import { Button, Confirm, Grid } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

export default class ActionBlock extends React.Component {
  state = { open: false };

  open = () => this.setState({ open: true });
  close = () => this.setState({ open: false });

  onLogoutAllConfirm = () => {
    this.props.logoutAll();
  };

  render() {
    return (
      <Grid textAlign="center">
        <Confirm
          open={this.state.open}
          onCancel={this.close}
          onConfirm={this.onLogoutAllConfirm}
          header="Logout on all devices"
          size="tiny"
          centered={false}
        />
        <Grid.Row>
          <Grid.Column>
            <Button.Group widths="2" size="large">
              <Button as={Link} to={'/profile/edit'} color="blue">
                Edit profile
              </Button>
              <Button.Or />
              <Button as={Link} to={'/profile/password'} color="teal">
                Change password
              </Button>
            </Button.Group>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Button.Group vertical labeled icon size="large">
              <Button
                icon="sign-out"
                color="orange"
                content="Logout on all devices"
                onClick={this.open}
              />
              <Button icon="external" content="Another action" />
              <Button icon="sync" content="Another action 2" />
            </Button.Group>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

import React from 'react';
import { Segment, Header } from 'semantic-ui-react';
import { connect } from 'react-redux';

import PasswordForm from './PasswordForm';
import { changePassword } from '../../actions/profileActions';

class ChangePassword extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Header
          as="h2"
          size="large"
          content="Change password"
          subheader="You can change your password"
        />
        <Segment>
          <PasswordForm
            loading={this.props.loading}
            onSubmit={formValues => this.props.changePassword(formValues)}
          />
        </Segment>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.app.loading,
  };
};

export default connect(
  mapStateToProps,
  { changePassword }
)(ChangePassword);

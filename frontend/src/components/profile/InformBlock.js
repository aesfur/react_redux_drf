import React from 'react';
import { Table } from 'semantic-ui-react';

export default ({ user }) => {
  return (
    <Table definition>
      <Table.Body>
        <Table.Row>
          <Table.Cell>Username</Table.Cell>
          <Table.Cell>{user.username || '-'}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>First name</Table.Cell>
          <Table.Cell>{user.first_name || '-'}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Last name</Table.Cell>
          <Table.Cell>{user.last_name || '-'}</Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell>Email</Table.Cell>
          <Table.Cell>{user.email || '-'}</Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  );
};

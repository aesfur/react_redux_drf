import React from 'react';
import { Button, Form } from 'semantic-ui-react';
import { Field, reduxForm } from 'redux-form';

import { Input } from '../formFields';
import { Link } from 'react-router-dom';

class PasswordForm extends React.Component {
  onSubmit = formValues => {
    this.props.onSubmit(formValues);
  };

  render() {
    return (
      <Form
        size="large"
        loading={this.props.loading}
        onSubmit={this.props.handleSubmit(this.onSubmit)}
      >
        <Field
          fluid
          name="old_password"
          component={Input}
          icon="key"
          iconPosition="left"
          placeholder="Current password"
          type="password"
        />
        <Field
          fluid
          name="new_password1"
          component={Input}
          icon="lock"
          iconPosition="left"
          placeholder="New password"
          type="password"
        />
        <Field
          fluid
          name="new_password2"
          component={Input}
          icon="lock"
          iconPosition="left"
          placeholder="New password confirmation"
          type="password"
        />
        <div style={{ textAlign: 'center' }}>
          <Button.Group size="large">
            <Button content="Cancel" as={Link} to="/profile" />
            <Button content="Submit" color="blue" type="submit" />
          </Button.Group>
        </div>
      </Form>
    );
  }
}

const validate = formValues => {
  const errors = {};
  const { old_password, new_password1, new_password2 } = formValues;

  if (!old_password) {
    errors.old_password = 'Please type in a current password';
  }

  if (!new_password1) {
    errors.new_password1 = 'Please enter a password';
  }

  if (!new_password2) {
    errors.new_password2 = 'Please enter a password';
  }

  if (new_password1 !== new_password2) {
    errors.new_password2 = 'Passwords are not equal';
  }

  if (new_password2 && new_password2.length < 8) {
    errors.new_password1 = 'Your password must contain at least 8 characters';
  }

  return errors;
};

export default reduxForm({
  form: 'register',
  validate,
})(PasswordForm);

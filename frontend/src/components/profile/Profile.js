import React from 'react';
import { Segment, Grid, Header } from 'semantic-ui-react';
import { connect } from 'react-redux';

import InformBlock from './InformBlock';
import ActionBlock from './ActionBlock';
import MyPostBlock from './MyPostBlock';
import { logoutAll } from '../../actions/authActions';

class Profile extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Header
          as="h2"
          size="large"
          content="Account Settings"
          subheader="Manage your account settings and set preferences"
        />
        <Grid divided="vertically">
          <Grid.Row columns={2}>
            <Grid.Column largeScreen={10} mobile={16}>
              <Segment size="large">
                <InformBlock user={this.props.user} />
              </Segment>
            </Grid.Column>
            <Grid.Column largeScreen={6} mobile={16}>
              <Segment>
                <ActionBlock logoutAll={this.props.logoutAll} />
              </Segment>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <Segment size="large">
                <MyPostBlock />
              </Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth.user,
});

export default connect(
  mapStateToProps,
  { logoutAll }
)(Profile);

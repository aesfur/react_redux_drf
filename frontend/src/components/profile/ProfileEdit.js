import React from 'react';
import { Segment, Header } from 'semantic-ui-react';
import { connect } from 'react-redux';

import ProfileForm from './ProfileForm';
import { updateUser } from '../../actions/profileActions';

class ProfileEdit extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Header
          as="h2"
          size="large"
          content="Profile Edit"
          subheader="Change your profile fields"
        />
        <Segment>
          <ProfileForm
            loading={this.props.loading}
            initialValues={this.props.user}
            onSubmit={formValues => this.props.updateUser(formValues)}
          />
        </Segment>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.app.loading,
    user: state.auth.user,
  };
};

export default connect(
  mapStateToProps,
  { updateUser }
)(ProfileEdit);

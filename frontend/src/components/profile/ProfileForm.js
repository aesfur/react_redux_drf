import React from 'react';
import { Form, Button, Table } from 'semantic-ui-react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';

import { Input } from '../formFields';

class ProfileForm extends React.Component {
  onSubmit = formValues => {
    this.props.onSubmit(formValues);
  };

  render() {
    const { loading } = this.props;
    return (
      <Form
        size="large"
        onSubmit={this.props.handleSubmit(this.onSubmit)}
        loading={loading}
      >
        <Table definition>
          <Table.Body>
            <Table.Row>
              <Table.Cell>Username</Table.Cell>
              <Table.Cell>
                <Field
                  name="username"
                  component={Input}
                  placeholder="Enter username"
                />
              </Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>First name</Table.Cell>
              <Table.Cell>
                <Field
                  name="first_name"
                  component={Input}
                  placeholder="Enter first name"
                />
              </Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Last name</Table.Cell>
              <Table.Cell>
                <Field
                  name="last_name"
                  component={Input}
                  placeholder="Enter last name"
                />
              </Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Email</Table.Cell>
              <Table.Cell>
                <Field
                  name="email"
                  type="email"
                  placeholder="Enter email"
                  readOnly
                  component={Input}
                />
              </Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>

        <div style={{ textAlign: 'center' }}>
          <Button.Group size="large">
            <Button content="Cancel" as={Link} to="/profile" />
            <Button content="Submit" color="blue" type="submit" />
          </Button.Group>
        </div>
      </Form>
    );
  }
}

const validate = formValues => {
  const errors = {};
  if (!formValues.username) errors.username = 'Please enter an username';
  return errors;
};

export default reduxForm({
  form: 'profileEdit',
  validate,
})(ProfileForm);

import React from 'react';
import { Icon, Label } from 'semantic-ui-react';

export default ({ tag, onRemove, index }) => {
  return (
    <Label as="a" color="blue" basic size="large">
      <Icon name="tag" />
      {tag}
      <Icon name="delete" onClick={() => onRemove(index)} />
    </Label>
  );
};

import { ALERT_SUCCESS, ALERT_ERROR, ALERT_CLEAR } from '../actions/types';

export default (state = [], action) => {
  switch (action.type) {
    case ALERT_SUCCESS:
      return [
        {
          color: 'green',
          message: action.payload,
        },
      ];

    case ALERT_ERROR:
      return action.payload.map(error => ({
        color: 'red',
        message: error,
      }));

    case ALERT_CLEAR:
      return [];

    default:
      return state;
  }
};

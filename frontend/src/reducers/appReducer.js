import {
  GET_POSTS_SUCCESS,
  GET_POST_SUCCESS,
  CREATE_POST_SUCCESS,
  DELETE_POST_SUCCESS,
  GET_USER_SUCCESS,
  LOADING,
  STOP_LOADING,
} from '../actions/types';

const INITIAL_STATE = {
  loading: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOADING:
      return { ...state, loading: true };

    case GET_POSTS_SUCCESS:
    case GET_POST_SUCCESS:
    case CREATE_POST_SUCCESS:
    case DELETE_POST_SUCCESS:
    case GET_USER_SUCCESS:
    case STOP_LOADING:
      return { ...state, loading: false };

    default:
      return state;
  }
};

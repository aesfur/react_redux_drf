import {
  GET_USER_SUCCESS,
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
  REGISTER_SUCCESS,
  AUTH_LOADING,
  AUTH_ERROR,
} from '../actions/types';

const INITIAL_STATE = {
  token: localStorage.getItem('token'),
  isAuthenticated: null,
  authLoading: true,
  user: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case AUTH_LOADING:
      return { ...state, authLoading: true };

    case GET_USER_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        authLoading: false,
        user: action.payload,
      };

    case LOGIN_SUCCESS:
    case REGISTER_SUCCESS:
      return {
        ...state,
        ...action.payload,
        isAuthenticated: true,
        authLoading: false,
      };

    case LOGOUT_SUCCESS:
    case AUTH_ERROR:
      return {
        ...state,
        token: null,
        user: null,
        isAuthenticated: false,
        authLoading: false,
      };

    default:
      return state;
  }
};

import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import postReducer from './postReducer';
import appReducer from './appReducer';
import authReducer from './authReducer';
import alertReducer from './alertReducer';
import tagsReducer from './tagsReducer';

export default combineReducers({
  posts: postReducer,
  app: appReducer,
  auth: authReducer,
  alerts: alertReducer,
  tags: tagsReducer,
  form: formReducer,
});

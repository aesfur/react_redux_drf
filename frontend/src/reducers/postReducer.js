import _ from 'lodash';
import {
  GET_POSTS_SUCCESS,
  GET_POST_SUCCESS,
  DELETE_POST_SUCCESS,
} from '../actions/types';

export default (state = {}, action) => {
  switch (action.type) {
    case GET_POSTS_SUCCESS:
      return { ...state, ..._.mapKeys(action.payload, 'id') };

    case GET_POST_SUCCESS:
      return { ...state, [action.payload.id]: action.payload };

    case DELETE_POST_SUCCESS:
      return _.omit(state, action.payload);

    default:
      return state;
  }
};

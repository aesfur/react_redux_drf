import { GET_TAGS_SUCCESS } from '../actions/types';

export default (state = [], action) => {
  switch (action.type) {
    case GET_TAGS_SUCCESS:
      return action.payload.tags;

    default:
      return state;
  }
};
